INTRODUCTION
------------

Decoupled Kit submodules allows to solve some tasks with decoupling Drupal:
- Object
- Block
- Menu
- Taxonomy
- Breadcrumb
- Sitemap
- Metatag
- Webform
- Redirect
- Router

The submodules created routes with JSON output.

The Openapi submodule returns an Open API description for all endpoints of the
module.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   Visit: https://www.drupal.org/node/1897420 for further information.

 * Enable the module and submodules.


REQUIREMENTS
-------------

 * Enable the modules that are required for the submodules.


CONFIGURATION
-------------

* Go to [Your Site]/admin/config/decoupled_kit
  or: Administration > Configuration > System > Decoupled Kit
  and configure enabled submodules on tabs.


MAINTAINERS
-----------

Current maintainers:
 * Serhii Klietsov (goodboy) - https://drupal.org/user/222910
