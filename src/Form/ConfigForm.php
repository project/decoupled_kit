<?php

namespace Drupal\decoupled_kit\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Config Form for the module.
 */
class ConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'decoupled_kit.config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('decoupled_kit.config');
    $form['path_prefix'] = [
      '#title' => $this->t('The path prefix for decoupled kit.'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => $config->get('path_prefix'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $prefix = $form_state->getValue('path_prefix');
    $this->config('decoupled_kit.config')
      ->set('path_prefix', trim($prefix, " /"))
      ->save();
  }

}
