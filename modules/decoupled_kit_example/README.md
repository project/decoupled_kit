INTRODUCTION
------------

Decoupled Kit Example is a skeleton Decoupled Kit module.
Need use ?path=[ALIAS].


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   Visit: https://www.drupal.org/node/1897420 for further information.

 * Enable module.


MAINTAINERS
-----------

Current maintainers:
 * Serhii Klietsov (goodboy) - https://drupal.org/user/222910
