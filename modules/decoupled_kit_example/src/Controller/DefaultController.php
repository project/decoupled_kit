<?php

namespace Drupal\decoupled_kit_example\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class Default Controller.
 */
class DefaultController extends ControllerBase {

  /**
   * Drupal\Core\DependencyInjection\ContainerInjectionInterface definition.
   *
   * @var \Drupal\Core\DependencyInjection\ContainerInjectionInterface
   */
  protected $decoupledKit;

  /**
   * Path alias manager.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $pathAliasManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->decoupledKit = $container->get('decoupled_kit');
    $instance->pathAliasManager = $container->has('path_alias.manager') ?
      $container->get('path_alias.manager') :
      $container->get('path.alias_manager');
    return $instance;
  }

  /**
   * Get metatag data for current page.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request object.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Return entity id data.
   */
  public function index(Request $request) {
    $path = $this->decoupledKit->checkPath($request);

    $array = [];
    $internal_path = $this->pathAliasManager->getPathByAlias($path);
    $entity = $this->decoupledKit->getEntityFromPath($internal_path);
    if ($entity) {
      $array = [
        'id' => $entity->id(),
        'type' => $entity->getEntityTypeId(),
        'bundle' => $entity->bundle(),
      ];
    }

    return new JsonResponse($array);
  }

}
