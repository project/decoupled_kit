INTRODUCTION
------------

Decoupled Kit Router creates decoupled_kit_router entity.
The submodule receives a router reference from its own object for the
Decoupled Kit object submodule. This allows to create own custom
references, instead of using JSON:API ones.

For example: JSON:API received links like /jsonapi/node/article/[UUID] for node
articles. Instead, you can use own custom link like /custom_node_article/[UUID]
or /custom/node/article/[UUID]. This custom route needs to be implemented.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   Visit: https://www.drupal.org/node/1897420 for further information.

 * Enable module.


REQUIREMENTS
-------------

 * Enable the modules that are required for the module.


CONFIGURATION
-------------

* Go to [Your Site]/admin/config/decoupled_kit/sitemap/config
  or: Administration > Configuration > System > Decoupled Kit >
  Decoupled Sitemap and configure on form.

* Go to [Your site]/admin/structure/decoupled-kit-router
  or: Administration > Structure > Decoupled Kit Routers and
  manage the routers list.


MAINTAINERS
-----------

Current maintainers:
 * Serhii Klietsov (goodboy) - https://drupal.org/user/222910
