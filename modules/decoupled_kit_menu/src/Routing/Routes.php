<?php

namespace Drupal\decoupled_kit_menu\Routing;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\Routing\Route;

/**
 * Defines dynamic routes.
 */
class Routes extends ControllerBase {

  /**
   * Routes data for decoupled_kit links.
   *
   * @return array
   *   Route data.
   */
  public function route() {
    $routes = [];
    $prefix = $this->config('decoupled_kit.config')->get('path_prefix');

    $routes['decoupled_kit_menu.index'] = new Route(
      '/' . $prefix . '/menu/{id}',
      [
        '_controller' => '\Drupal\decoupled_kit_menu\Controller\DefaultController::index',
        '_title' => 'index',
      ],
      [
        '_permission'  => 'access content',
      ]
    );
    return $routes;
  }

}
