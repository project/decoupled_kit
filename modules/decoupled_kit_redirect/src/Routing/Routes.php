<?php

namespace Drupal\decoupled_kit_redirect\Routing;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\Routing\Route;

/**
 * Defines dynamic routes.
 */
class Routes extends ControllerBase {

  /**
   * Routes data for decoupled_kit links.
   *
   * @return array
   *   Route data.
   */
  public function route() {
    $routes = [];
    $prefix = $this->config('decoupled_kit.config')->get('path_prefix');

    $routes['decoupled_kit_redirect.index'] = new Route(
      '/' . $prefix . '/redirect',
      [
        '_controller' => '\Drupal\decoupled_kit_redirect\Controller\DefaultController::index',
        '_title' => 'index',
      ],
      [
        '_permission'  => 'access content',
        '_format' => 'json',
      ]
    );
    $routes['decoupled_kit_redirect.config'] = new Route(
      '/admin/config/decoupled_kit/redirect/config',
      [
        '_form' => '\Drupal\decoupled_kit_redirect\Form\ConfigForm',
        '_title' => 'Decoupled Redirect configuration',
      ],
      [
        '_permission'  => 'access content',
      ]
    );
    return $routes;
  }

}
