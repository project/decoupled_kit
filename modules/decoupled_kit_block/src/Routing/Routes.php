<?php

namespace Drupal\decoupled_kit_block\Routing;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\Routing\Route;

/**
 * Defines dynamic routes.
 */
class Routes extends ControllerBase {

  /**
   * Routes data for decoupled_kit links.
   *
   * @return array
   *   Route data.
   */
  public function route() {
    $routes = [];
    $prefix = $this->config('decoupled_kit.config')->get('path_prefix');

    $routes['decoupled_kit_block.index'] = new Route(
      '/' . $prefix . '/block',
      [
        '_controller' => '\Drupal\decoupled_kit_block\Controller\DefaultController::index',
        '_title' => 'index',
      ],
      [
        '_permission'  => 'access content',
      ]
    );
    $routes['decoupled_kit_block.data'] = new Route(
      '/' . $prefix . '/block/data/{plugin}',
      [
        '_controller' => '\Drupal\decoupled_kit_block\Controller\DefaultController::getJsonData',
        '_title' => 'Blocks data',
      ],
      [
        '_permission'  => 'access content',
      ]
    );
    $routes['decoupled_kit_block.config'] = new Route(
      '/admin/config/decoupled_kit/block/config',
      [
        '_form' => '\Drupal\decoupled_kit_block\Form\ConfigForm',
        '_title' => 'Decoupled Block configuration',
      ],
      [
        '_permission'  => 'administer site configurations',
      ],
      [
        '_admin_route' => TRUE,
      ]
    );
    return $routes;
  }

}
