<?php

namespace Drupal\decoupled_kit_object\Routing;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\Routing\Route;

/**
 * Defines dynamic routes.
 */
class Routes extends ControllerBase {

  /**
   * Routes data for decoupled_kit links.
   *
   * @return array
   *   Route data.
   */
  public function route() {
    $routes = [];
    $prefix = $this->config('decoupled_kit.config')->get('path_prefix');

    $routes['decoupled_kit_object.index'] = new Route(
      '/' . $prefix . '/object',
      [
        '_controller' => '\Drupal\decoupled_kit_object\Controller\DefaultController::index',
        '_title' => 'index',
      ],
      [
        '_permission'  => 'access content',
        '_format' => 'json',
      ]
    );
    return $routes;
  }

}
