INTRODUCTION
------------

Decoupled Kit Object get entity object data for current page.
Need use ?path=[ALIAS]. Access to an object is checked.

Available routers:
  - Decoupled Kit Router (decoupled_kit_router module)
  - JSON:API router (JSON:API module)


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   Visit: https://www.drupal.org/node/1897420 for further information.

 * Enable module.


MAINTAINERS
-----------

Current maintainers:
 * Serhii Klietsov (goodboy) - https://drupal.org/user/222910
