<?php

namespace Drupal\decoupled_kit_taxonomy\Routing;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\Routing\Route;

/**
 * Defines dynamic routes.
 */
class Routes extends ControllerBase {

  /**
   * Routes data for decoupled_kit links.
   *
   * @return array
   *   Route data.
   */
  public function route() {
    $routes = [];
    $prefix = $this->config('decoupled_kit.config')->get('path_prefix');

    $routes['decoupled_kit_taxonomy.index'] = new Route(
      '/' . $prefix . '/taxonomy/{id}',
      [
        '_controller' => '\Drupal\decoupled_kit_taxonomy\Controller\DefaultController::index',
        '_title' => 'index',
      ],
      [
        '_permission'  => 'access content',
      ]
    );
    $routes['decoupled_kit_taxonomy.config'] = new Route(
      '/admin/config/decoupled_kit/taxonony/config',
      [
        '_form' => '\Drupal\decoupled_kit_taxonomy\Form\ConfigForm',
        '_title' => 'Decoupled Taxonomy configuration',
      ],
      [
        '_permission'  => 'administer site configurations',
      ],
      [
        '_admin_route' => TRUE,
      ]
    );
    return $routes;
  }

}
