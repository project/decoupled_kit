INTRODUCTION
------------

Decoupled Kit Open API implementation.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   Visit: https://www.drupal.org/node/1897420 for further information.

 * Enable module.


REQUIREMENTS
-------------

 * Enable the modules that are required for the module.

 * Recommended to enable openapi_ui_redoc module.


CONFIGURATION
-------------

* Go to [Your Site]/admin/config/services/openapi
  or: Administration > Webservices > OpenAPI
  and read the Open API descriptions for the Decoupled Kit module.


MAINTAINERS
-----------

Current maintainers:
 * Serhii Klietsov (goodboy) - https://drupal.org/user/222910
