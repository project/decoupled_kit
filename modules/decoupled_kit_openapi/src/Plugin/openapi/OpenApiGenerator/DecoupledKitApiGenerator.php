<?php

namespace Drupal\decoupled_kit_openapi\Plugin\openapi\OpenApiGenerator;

use Drupal\Core\Authentication\AuthenticationCollectorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\Core\Url;
use Drupal\openapi\Plugin\openapi\OpenApiGeneratorBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Defines an OpenApi Schema Generator for the Decoupled Kit endpoints.
 *
 * @OpenApiGenerator(
 *   id = "decoupled_kit",
 *   label = @Translation("Decoupled Kit"),
 * )
 */
class DecoupledKitApiGenerator extends OpenApiGeneratorBase {

  /**
   * The module handler..
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * DecoupledKitApiGenerator constructor.
   *
   * @param array $configuration
   *   Plugin configuration.
   * @param string $plugin_id
   *   Unique plugin id.
   * @param array|mixed $plugin_definition
   *   Plugin instance definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Routing\RouteProviderInterface $routing_provider
   *   The routing provider.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $field_manager
   *   The field manager.
   * @param \Symfony\Component\Serializer\SerializerInterface $serializer
   *   The serializer.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The current request stack.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration object factory.
   * @param \Drupal\Core\Authentication\AuthenticationCollectorInterface $authentication_collector
   *   The authentication collector.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, RouteProviderInterface $routing_provider, EntityFieldManagerInterface $field_manager, SerializerInterface $serializer, RequestStack $request_stack, ConfigFactoryInterface $config_factory, AuthenticationCollectorInterface $authentication_collector, ModuleHandlerInterface $module_handler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $routing_provider, $field_manager, $serializer, $request_stack, $config_factory, $authentication_collector);

    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('router.route_provider'),
      $container->get('entity_field.manager'),
      $container->get('serializer'),
      $container->get('request_stack'),
      $container->get('config.factory'),
      $container->get('authentication_collector'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getBasePath() {
    return parent::getBasePath() . '/' . $this->getUpsApiBase();
  }

  /**
   * Determine the base for the endpoints routes.
   *
   * @return string
   *   The url prefix used for endpoints.
   */
  public function getUpsApiBase() {
    $config = $this->configFactory->get('decoupled_kit.config');
    return $config->get('path_prefix');
  }

  /**
   * {@inheritdoc}
   */
  public function getPaths() {
    $api_paths = [];

    if ($this->moduleHandler->moduleExists('decoupled_kit_object')) {
      $path = Url::fromRoute('decoupled_kit_object.index')->toString();
      $api_paths[$path] = [
        'get' => [
          'summary' => $this->t('Object data'),
          'description' => $this->t('Get JSON for current path.'),
          'parameters' => [
            0 => [
              'in' => 'query',
              'name' => 'path',
              'required' => TRUE,
              'type' => 'string',
              'description' => $this->t('Path'),
            ],
          ],
          'tags' => ['Object'],
        ],
      ];
    }

    if ($this->moduleHandler->moduleExists('decoupled_kit_block')) {
      $path = Url::fromRoute('decoupled_kit_block.index')->toString();
      $api_paths[$path] = [
        'get' => [
          'summary' => $this->t('Blocks data'),
          'description' => $this->t('Get blocks list for current path.'),
          'parameters' => [
            0 => [
              'in' => 'query',
              'name' => 'path',
              'required' => TRUE,
              'type' => 'string',
              'description' => $this->t('Path'),
            ],
          ],
          'tags' => ['Block'],
        ],
      ];

      $path = Url::fromRoute('decoupled_kit_block.data', [
        'plugin' => 'PLUGIN',
      ])->toString();
      $api_paths[$path] = [
        'get' => [
          'summary' => $this->t('Block data'),
          'description' => $this->t('Get block JSON.'),
          'parameters' => [
            0 => [
              'in' => 'path',
              'name' => 'plugin',
              'required' => TRUE,
              'type' => 'string',
              'description' => $this->t('Block plugin'),
            ],
          ],
          'tags' => ['Block'],
        ],
      ];
    }

    if ($this->moduleHandler->moduleExists('decoupled_kit_breadcrumb')) {
      $path = Url::fromRoute('decoupled_kit_breadcrumb.index')->toString();
      $api_paths[$path] = [
        'get' => [
          'summary' => $this->t('Breadcrumb data'),
          'description' => $this->t('Get JSON breadcrumb for current path.'),
          'parameters' => [
            0 => [
              'in' => 'query',
              'name' => 'path',
              'required' => TRUE,
              'type' => 'string',
              'description' => $this->t('Path'),
            ],
          ],
          'tags' => ['Breadcrumb'],
        ],
      ];
    }

    if ($this->moduleHandler->moduleExists('decoupled_kit_menu')) {
      $path = Url::fromRoute('decoupled_kit_menu.index', [
        'id' => 'ID',
      ])->toString();
      $api_paths[$path] = [
        'get' => [
          'summary' => $this->t('Menu data'),
          'description' => $this->t('Get menu JSON.'),
          'parameters' => [
            0 => [
              'in' => 'path',
              'name' => 'id',
              'required' => TRUE,
              'type' => 'string',
              'description' => $this->t('Menu ID'),
            ],
          ],
          'tags' => ['Menu'],
        ],
      ];
    }

    if ($this->moduleHandler->moduleExists('decoupled_kit_metatag')) {
      $path = Url::fromRoute('decoupled_kit_metatag.index')->toString();
      $api_paths[$path] = [
        'get' => [
          'summary' => $this->t('Metatag data'),
          'description' => $this->t('Get JSON metatag for current path.'),
          'parameters' => [
            0 => [
              'in' => 'query',
              'name' => 'path',
              'required' => TRUE,
              'type' => 'string',
              'description' => $this->t('Path'),
            ],
          ],
          'tags' => ['Metatag'],
        ],
      ];
    }

    if ($this->moduleHandler->moduleExists('decoupled_kit_redirect')) {
      $path = Url::fromRoute('decoupled_kit_redirect.index')->toString();
      $api_paths[$path] = [
        'get' => [
          'summary' => $this->t('Redirect data'),
          'description' => $this->t('Get JSON redirect for current path.'),
          'parameters' => [
            0 => [
              'in' => 'query',
              'name' => 'path',
              'required' => TRUE,
              'type' => 'string',
              'description' => $this->t('Path'),
            ],
          ],
          'tags' => ['Redirect'],
        ],
      ];
    }

    if ($this->moduleHandler->moduleExists('decoupled_kit_sitemap')) {
      $path = Url::fromRoute('decoupled_kit_sitemap.index')->toString();
      $api_paths[$path] = [
        'get' => [
          'summary' => $this->t('Sitemap data'),
          'description' => $this->t('Get JSON sitemap for current path.'),
          'parameters' => [
            0 => [
              'in' => 'query',
              'name' => 'path',
              'required' => TRUE,
              'type' => 'string',
              'description' => $this->t('Path'),
            ],
          ],
          'tags' => ['Sitemap'],
        ],
      ];
    }

    if ($this->moduleHandler->moduleExists('decoupled_kit_taxonomy')) {
      $path = Url::fromRoute('decoupled_kit_taxonomy.index', [
        'id' => 'ID',
      ])->toString();
      $api_paths[$path] = [
        'get' => [
          'summary' => $this->t('Taxonomy data'),
          'description' => $this->t('Get taxonomy JSON.'),
          'parameters' => [
            0 => [
              'in' => 'path',
              'name' => 'id',
              'required' => TRUE,
              'type' => 'string',
              'description' => $this->t('Taxonomy ID'),
            ],
          ],
          'tags' => ['Taxonomy'],
        ],
      ];
    }

    if ($this->moduleHandler->moduleExists('decoupled_kit_webform')) {
      $path = Url::fromRoute('decoupled_kit_webform.index', [
        'id' => 'ID',
      ])->toString();
      $api_paths[$path] = [
        'get' => [
          'summary' => $this->t('Webform data'),
          'description' => $this->t('Get webform JSON.'),
          'parameters' => [
            0 => [
              'in' => 'path',
              'name' => 'id',
              'required' => TRUE,
              'type' => 'string',
              'description' => $this->t('Webform ID'),
            ],
          ],
          'tags' => ['Webform'],
        ],
      ];
    }

    return $api_paths;
  }

  /**
   * {@inheritdoc}
   */
  protected function getJsonSchema($described_format, $entity_type_id, $bundle_name = NULL) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getTags() {
    $tags = [];

    if ($this->moduleHandler->moduleExists('decoupled_kit_object')) {
      $tags[] = [
        'name' => 'Object',
        'description' => $this->t('Decoupled Object'),
      ];
    }

    if ($this->moduleHandler->moduleExists('decoupled_kit_block')) {
      $tags[] = [
        'name' => 'Block',
        'description' => $this->t('Decoupled Menu'),
      ];
    }

    if ($this->moduleHandler->moduleExists('decoupled_kit_breadcrumb')) {
      $tags[] = [
        'name' => 'Breadcrumb',
        'description' => $this->t('Decoupled Breadcrumb'),
      ];
    }

    if ($this->moduleHandler->moduleExists('decoupled_kit_menu')) {
      $tags[] = [
        'name' => 'Menu',
        'description' => $this->t('Decoupled Menu'),
      ];
    }

    if ($this->moduleHandler->moduleExists('decoupled_kit_metatag')) {
      $tags[] = [
        'name' => 'Metatag',
        'description' => $this->t('Decoupled Metatag'),
      ];
    }

    if ($this->moduleHandler->moduleExists('decoupled_kit_redirect')) {
      $tags[] = [
        'name' => 'Redirect',
        'description' => $this->t('Decoupled Redirect'),
      ];
    }

    if ($this->moduleHandler->moduleExists('decoupled_kit_sitemap')) {
      $tags[] = [
        'name' => 'Sitemap',
        'description' => $this->t('Decoupled Sitemap'),
      ];
    }

    if ($this->moduleHandler->moduleExists('decoupled_kit_taxonomy')) {
      $tags[] = [
        'name' => 'Taxonomy',
        'description' => $this->t('Decoupled Taxonomy'),
      ];
    }

    if ($this->moduleHandler->moduleExists('decoupled_kit_webform')) {
      $tags[] = [
        'name' => 'Webform',
        'description' => $this->t('Decoupled Webform'),
      ];
    }

    return $tags;
  }

  /**
   * {@inheritdoc}
   */
  public function getConsumes() {
    return [
      'application/vnd.api+json',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getProduces() {
    return [
      'application/vnd.api+json',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getApiName() {
    return $this->t('Decoupled Kit API');
  }

  /**
   * {@inheritdoc}
   */
  protected function getApiDescription() {
    return $this->t('This is a Decoupled Kit API compliant implementation');
  }

}
