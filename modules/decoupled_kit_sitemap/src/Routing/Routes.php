<?php

namespace Drupal\decoupled_kit_sitemap\Routing;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\Routing\Route;

/**
 * Defines dynamic routes.
 */
class Routes extends ControllerBase {

  /**
   * Routes data for decoupled_kit links.
   *
   * @return array
   *   Route data.
   */
  public function route() {
    $routes = [];
    $prefix = $this->config('decoupled_kit.config')->get('path_prefix');

    $routes['decoupled_kit_sitemap.index'] = new Route(
      '/' . $prefix . '/sitemap',
      [
        '_controller' => '\Drupal\decoupled_kit_sitemap\Controller\DefaultController::index',
        '_title' => 'index',
      ],
      [
        '_permission'  => 'access content',
      ]
    );
    $routes['decoupled_kit_sitemap.config'] = new Route(
      '/admin/config/decoupled_kit/sitemap/config',
      [
        '_form' => '\Drupal\decoupled_kit_sitemap\Form\ConfigForm',
        '_title' => 'Decoupled Sitemap configuration',
      ],
      [
        '_permission'  => 'administer site configurations',
      ],
      [
        '_admin_route' => TRUE,
      ]
    );
    return $routes;
  }

}
