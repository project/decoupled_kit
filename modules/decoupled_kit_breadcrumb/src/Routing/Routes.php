<?php

namespace Drupal\decoupled_kit_breadcrumb\Routing;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\Routing\Route;

/**
 * Defines dynamic routes.
 */
class Routes extends ControllerBase {

  /**
   * Routes data for decoupled_kit links.
   *
   * @return array
   *   Route data.
   */
  public function route() {
    $routes = [];
    $prefix = $this->config('decoupled_kit.config')->get('path_prefix');

    $routes['decoupled_kit_breadcrumb.index'] = new Route(
      '/' . $prefix . '/breadcrumb',
      [
        '_controller' => '\Drupal\decoupled_kit_breadcrumb\Controller\DefaultController::index',
        '_title' => 'index',
      ],
      [
        '_permission'  => 'access content',
      ]
    );
    $routes['decoupled_kit_breadcrumb.config'] = new Route(
      '/admin/config/decoupled_kit/breadcrumb/config',
      [
        '_form' => '\Drupal\decoupled_kit_breadcrumb\Form\ConfigForm',
        '_title' => 'Decoupled Breadcrumb configuration',
      ],
      [
        '_permission'  => 'administer site configurations',
      ],
      [
        '_admin_route' => TRUE,
      ]
    );
    return $routes;
  }

}
